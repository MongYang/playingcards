/*
Mong Yang
Instructor Ryan Appel
Computer Programming C++
2/8/2010

Exercise 2: Playing Cards
repository of Cole Werkmeister
*/
#include <iostream>
#include <conio.h>
#include <string>
#include <stdlib.h> // rand() and srand();
#include <ctime> // time()

using namespace std;

enum Suit
{
	CLUBS, DIAMONDS, HEARTS, SPADES
};

enum Rank
{
	ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
};

struct Card
{
	Suit suit;
	Rank rank;
};

// Draws a random card
Card DrawCard(Card &card)
{
	srand(time(0));
	int randrank = (rand() % 13); // Range: 0 - 12.
	srand(time(0));
	int randsuit = (rand() % 4);  // Range: 0 - 3
	card.rank = (Rank)randrank;
	card.suit = (Suit)randsuit;

	return card;
}

// Prints card
void PrintCard(Card &name)
{	
	// Prints the Rank then prints the Suit.
	switch (name.rank)
	{
	case 0:
		cout << "ACE";
		break;
	case 1:
		cout << "TWO";
		break;
	case 2:
		cout << "THREE";
		break;
	case 3:
		cout << "FOUR";
		break;
	case 4:
		cout << "FIVE";
		break;
	case 5:
		cout << "SIX";
		break;
	case 6:
		cout << "SEVEN";
		break;
	case 7:
		cout << "EIGHT";
		break;
	case 8:
		cout << "NINE";
		break;
	case 9:
		cout << "TEN";
		break;
	case 10:
		cout << "JACK";
		break;
	case 11:
		cout << "QUEEN";
		break;
	case 12:
		cout << "KING";
		break;
	default:
		break;
	}

	cout << " of ";

	switch (name.suit)
	{
	case 0:
		cout << "CLUBS";
		break;
	case 1:
		cout << "DIAMONDS";
		break;
	case 2:
		cout << "HEARTS";
		break;
	case 3:
		cout << "SPADES";
		break;
	default:
		break;
	}

	cout << "\n";
}

// Compares two cards and returns high card
Card HighCard(Card handA, Card handB)
{
	Suit suitA = handA.suit;
	Suit suitB = handB.suit;

	Rank rankA = handA.rank;
	Rank rankB = handB.rank;

	if (handA.suit == handB.suit)  // Ranks are only compared when suits are equal.
	{
		if (handA.rank == 0) return handA;		// Because ACE equals 0, this condition compares which ever card has 
		else if (handB.rank == 0) return handB; // a rank of ACE to be the high card.
		else if (handA.rank > handB.rank) return handA; // Compares the ranks from TWO to KING.
		else if (handA.rank < handB.rank) return handB;
	}
	else
	{
		if (handA.suit > handB.suit) return handA;  // Compare Suits.
		else return handB;
	}
}

bool Reset();

int main()
{
	bool run = false;

	do {

	char input1;
	cout << "Enter any single key to draw the first card. ";
	cin >> input1;
	cout << "Hand #1:  " << "\n";
	Card hand1;
	DrawCard(hand1);
	PrintCard(hand1);
	cout << "\n";

	char input2;
	cout << "Enter any single key to draw the second card. ";
	cin >> input2;
	cout << "Hand #2:  " << "\n";
	Card hand2;
	DrawCard(hand2);
	// Call the DrawCard() function again if both hands are the same.
	while ((hand1.rank == hand2.rank) && (hand1.suit == hand2.suit))
	{
		DrawCard(hand2);
	}
	PrintCard(hand2);
	cout << "\n";

	Card High = HighCard(hand1, hand2);
	cout << "The high card is:  ";
	PrintCard(High);
	cout << "\n";

	run = Reset();

	} while (run);

	_getch();
	return 0;
}

bool Reset()
{
	char input;
	cout << "Play again?\nType y for Yes and any other key to leave.\n";
	cin >> input;
	cout << "\n\n";

	if (input == 'y' || input == 'Y') return true;
	else return false;
}