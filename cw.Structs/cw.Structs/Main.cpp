#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum DoorState
{
	OPEN, CLOSING, CLOSED, OPENING
};

struct Door
{
	DoorState state;
	bool isLocked;
};

struct Student
{
	string firstname;
	string lastname;
	float gpa;
};

int main()
{
	Student s1;
	s1.firstname = "Cole";
	s1.lastname = "Werkenhamen";
	s1.gpa = 4.0f;

	_getch();
	return 0;
}